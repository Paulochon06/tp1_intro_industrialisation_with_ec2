resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDxRvZzii+gxkpXqFNDIrnZJX/+dsXk/M91GHseyNjMzYtQkiknmSYZ+gNltI2twPgljVcMgFSp1Yn2WeqBthyTDgiCW+rUnfQleMOztfoOSNQOJRLmsGGvhjOc1ph0urPB2ayB8Zczt1oUN93TYeHObqVWhQPm93jBD0G/UO+R44IOQHPPvs54VNZwpRs38vNUDc9nJ6R7hGBmVaOQI/8MBv2iEsHUjdCEdzv8V2dd7AvCSVc+aUhab5RtZ49C0qxvhZTjQQ5THqjCaCw/ekliNjIKwc1tNWJvcZ2+bZwduVqtpS89AycJ9zY3HuRK2Cw9HcpyFBNWdmlKrbov4g/k+XeEUFV0CXFFCGAKec2fnZVXZbfSIWqN2IzSy7aJ4oO5fi+ep9H/cxdF+dnmPYm2aYB3W6hW8VqEcEwiGJ2oT0sNMnM18RkuGntzsStWqfeg2vQJv4Ew5JgfKSz6wcchq6EQM7pdS5GCNa4wKQkc/tWrlWL/sV594JZz0h9bfys= monteverde@monteverde"
  #key_name = "AWS-INSTANCE"
  tags = {
    Name = "Default VPC"
  }
  
}

resource "aws_instance" "EC2" {
  ami = var.ami_id
  instance_type = var.instance_type
  #key_name = [aws_key_pair.deployer]
  security_groups = [aws_security_group.allow_ssh.name]
}

resource "aws_security_group" "allow_ssh" {
  name = "allow_ssh"
  description = "allw ssh inbound/outbound traffic"
  vpc_id = "vpc-69915810"

  ingress {
    from_port = 22
    to_port = 22
    protocol  = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}